package com.example.uttam.dynamicbutton3;


import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    List<String> ItemsList;
    TextView DisplaySelectedItem;
    String selectedItem;
    String[] itemsName;
    int n;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Random rand = new Random();
        n = rand.nextInt((40 - 5) + 1) + 5;
        Log.d("btn", "Clicked Button Number: "+n);

        itemsName=new String[n];

       // microphoneVisible(n);

        for(int i=0;i<n;i++){
//            gridArray.add(new ClipData.Item("Button_"+(i+1)));
            itemsName[i]="Button_"+(i+1);
        }

        gridView = (GridView) findViewById(R.id.gridView1);

        DisplaySelectedItem = (TextView)findViewById(R.id.textView1);

        ItemsList = new ArrayList<String>(Arrays.asList(itemsName));

        gridView.setAdapter(new TextAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                selectedItem = parent.getItemAtPosition(position).toString();

                DisplaySelectedItem.setText(selectedItem +" is selected.");

            }
        });

    }



    private class TextAdapter extends BaseAdapter
    {

        Context context;

        public TextAdapter(Context context)
        {
            this.context = context;
        }

        @Override
        public int getCount() {

            return itemsName.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub

            return itemsName[position];
        }

        @Override
        public long getItemId(int position) {

            // TODO Auto-generated method stub

            return position;
        }

        @SuppressLint("ResourceType")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            TextView text = new TextView(this.context);

            text.setText(itemsName[position]);

            text.setGravity(Gravity.CENTER);
//            text.setWidth(200);
//            text.setHeight(100);
            text.setPadding(10,0,10,0);
            text.setSingleLine(true);
            text.setEllipsize(TextUtils.TruncateAt.END);
            text.setId(position+1);
            text.setTextColor(getResources().getColor(R.color.btnTextColor));
            text.setBackgroundResource(R.drawable.button_shape);
            //text.setBackgroundResource(R.layout.text_view_grid);


            LinearLayout.LayoutParams prms = new LinearLayout.LayoutParams(200,85);
            prms.setMargins(10, 15, 10, 15);
            text.setLayoutParams(prms);

            //GridView.LayoutParams lp=new GridView.LayoutParams(200, 85);
            //lp.setMargins();
            //text.setLayoutParams(lp);

            return text;

        }


    }
}